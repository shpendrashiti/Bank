core.migrations package
=======================

Submodules
----------

core.migrations.0001\_initial module
------------------------------------

.. automodule:: core.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:

core.migrations.0002\_transaction\_creator module
-------------------------------------------------

.. automodule:: core.migrations.0002_transaction_creator
   :members:
   :undoc-members:
   :show-inheritance:

core.migrations.0003\_remove\_transaction\_is\_approved\_transaction\_status module
-----------------------------------------------------------------------------------

.. automodule:: core.migrations.0003_remove_transaction_is_approved_transaction_status
   :members:
   :undoc-members:
   :show-inheritance:

core.migrations.0004\_alter\_transaction\_status module
-------------------------------------------------------

.. automodule:: core.migrations.0004_alter_transaction_status
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: core.migrations
   :members:
   :undoc-members:
   :show-inheritance:
