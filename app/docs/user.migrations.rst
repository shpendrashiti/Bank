user.migrations package
=======================

Submodules
----------

user.migrations.0001\_initial module
------------------------------------

.. automodule:: user.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:

user.migrations.0002\_user\_city\_user\_country\_user\_dob\_user\_surname module
--------------------------------------------------------------------------------

.. automodule:: user.migrations.0002_user_city_user_country_user_dob_user_surname
   :members:
   :undoc-members:
   :show-inheritance:

user.migrations.0003\_user\_email\_alter\_user\_username module
---------------------------------------------------------------

.. automodule:: user.migrations.0003_user_email_alter_user_username
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: user.migrations
   :members:
   :undoc-members:
   :show-inheritance:
